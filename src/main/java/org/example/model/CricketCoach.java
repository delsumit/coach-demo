package org.example.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CricketCoach implements Coach {

    private String coachId;
    private String coachName;
    private FortuneService fortuneService;

    public CricketCoach(String coachId, String coachName) {
        this.coachId = coachId;
        this.coachName = coachName;
    }

    @Override
    public String getDailyWorkout() {
        return "Todays spin bowl";
    }
}
